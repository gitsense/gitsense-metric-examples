--
-- Get the weekly lines of code metrics for all *.md files on the master branch
--

CREATE TEMP TABLE latest(head_id INTEGER);

INSERT INTO latest
    SELECT
        commits.id
    FROM
        z1_branches_1 AS branches,
        z1_commits_1 AS commits
    WHERE
        branches.branch='master' AND
        branches.head=commits.name;

CREATE TEMP TABLE loc(loc BIGINT, root_commit_id INTEGER);

INSERT INTO loc
    SELECT
        SUM(loc),
        trees.root_commit_id
    from
        z1_trees_1 AS trees,
        z1_changes_1 AS changes,
        z1_file_stat_1 AS file_stat,
        z1_paths_1 AS paths 
    where
        changes.id=trees.change_id AND
        file_stat.id=changes.file_stat_id AND
        paths.path LIKE '%.md' AND 
        changes.path_id=paths.id AND 
        trees.root_commit_id in (
            SELECT
                id
            FROM
                z1_commits_1 AS commits,
                z1_timelines_1 AS timelines,
                latest
            WHERE
                timelines.head_id=latest.head_id AND
                timelines.root_commit_id=commits.id
        ) GROUP BY trees.root_commit_id;

SELECT
    label AS week,
    loc
FROM
    z1_timelines_1 AS timelines,
    loc,
    latest
WHERE
    timelines.type='weekly' AND
    timelines.head_id=latest.head_id and
    timelines.root_commit_id=loc.root_commit_id
ORDER BY order_idx DESC
