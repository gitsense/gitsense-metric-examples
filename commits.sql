--
-- Displays the top 5 committers
--

SELECT 
    author_email,
    COUNT(id) AS num_commits
FROM 
    z1_commits_1 GROUP BY author_email ORDER BY num_commits DESC LIMIT 5;

--
-- Display the top 5 committers, excluding empty merge commits
--

SELECT 
    author_email,
    COUNT(id) AS num_commits
FROM 
    z1_commits_1
WHERE
    num_chgs != 0 GROUP BY author_email ORDER BY num_commits DESC LIMIT 5;

--
-- Display the top 5 committers who modified at least one *.md file 
--

SELECT 
    author_email,
    COUNT(id) AS num_commits
FROM 
    z1_commits_1 AS commits
WHERE
    id IN (
        SELECT 
            DISTINCT(commit_id)
        FROM 
            z1_changes_1 AS changes,
            z1_paths_1 AS paths
        WHERE 
            changes.path_id=paths.id AND 
            paths.path LIKE '%.md'
    ) GROUP BY author_email ORDER BY num_commits DESC LIMIT 5;
